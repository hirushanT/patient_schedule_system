<?php

/* main chart php file. dont change or delete this */

   include("includes/chartAPI.php");

/* wenas kalata kamak na. dont use a separate file. unless it's not working i dont know why. */

   $hostdb = "localhost";
   $userdb = "root";
   $passdb = "";
   $namedb = "";

   $dbhandle = new mysqli($hostdb, $userdb, $passdb, $namedb);

   /*error msg  */
   if ($dbhandle->connect_error) {
  	exit("connection Error: ".$dbhandle->connect_error);
   }
?>

<html>
   <head>
  	<title>Patient charts</title>
    <link  rel="stylesheet" type="text/css" href="css/style.css" />

  	<!--FUSHIONCHART API -->

  	<script src="fusioncharts.js"></script>
  </head>

   <body>
  	<?php

     	// DATA SPACE TO ADD PAST 10 MONTHS REPORT ACTIVITIES
     	$strQuery = "SELECT patientName, results FROM Months ORDER BY Months DESC LIMIT 10";

     	// EXECUTING AND ERROR MSG.
     	$result = $dbhandle->query($strQuery) or exit("Error code ({$dbhandle->errno}): {$dbhandle->error}");

     	// IF VALID TROW TO JASON
     	if ($result) {
        	// The `$arrData` CHART ATTRIBUTES AND DATA
        	$arrData = array(
        	    "chart" => array(
                  "caption" => "Top 10 Most Populous Countries",
                  "showValues" => "0",
                  "theme" => "zune"
              	)
           	);

        	$arrData["data"] = array();

	// PUSH INTO ARRAY
        	while($row = mysqli_fetch_array($result)) {
           	array_push($arrData["data"], array(
              	"label" => $row["Name"],
              	"value" => $row["Population"]
              	)
           	);
        	}

        	/*RETRIEVE DATA IN JASON ARRY. */

        	$jsonEncodedData = json_encode($arrData);

	/*Using fusion chart API to add data to a chart.*/

        	$columnChart = new FusionCharts("column2D", "myFirstChart" , 600, 300, "chart-1", "json", $jsonEncodedData);

        	// Render chart
        	$columnChart->render();

        	// Close database connection
        	$dbhandle->close();
     	}

  	?>

  	<div id="chart-1"> </div>

   </body>

</html>
